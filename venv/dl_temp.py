import collections
import os
import io
import sys
import numpy as np
from PIL import Image
import cv2
import tensorflow as tf
from utils import get_dataset_colormap
import tarfile
from matplotlib import gridspec
from matplotlib import pyplot as plt

model_dir =''

IMAGE_DIR = '/Users/stephanie/PycharmProjects/tf-models-master/venv/workspace_training/training_demo/test_one/'


PATH_TO_CKPT = '/Users/stephanie/PycharmProjects/tf-models-master/venv/research/deeplab/pre-trained-models/frozen_inference_graph.pb'

PATH_TO_CKPT = '/Users/stephanie/Downloads/deeplabv3_pascal_trainval/frozen_inference_graph.pb'


#PATH_TO_CKPT = '/Users/stephanie/PycharmProjects/tf-models-master/venv/workspace_training/training_demo/training_120_new/frozen_inference_graph.pb'


INPUT_TENSOR_NAME = 'ImageTensor:0'
OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
INPUT_SIZE = 513

LABEL_NAMES = np.asarray([
    'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
    'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog',
    'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa',
    'train', 'tv'
])

FULL_LABEL_MAP = np.arange(len(LABEL_NAMES)).reshape(len(LABEL_NAMES), 1)
FULL_COLOR_MAP = get_dataset_colormap.label_to_color_image(FULL_LABEL_MAP)

_TARBALL_NAME = 'deeplab_model.tar.gz'
_FROZEN_GRAPH_NAME = 'frozen_inference_graph'

# detection_graph = tf.Graph()
# with detection_graph.as_default():
#   od_graph_def = tf.GraphDef()
#   with open(PATH_TO_CKPT, 'rb') as fid:
#     serialized_graph = fid.read()
#     od_graph_def.ParseFromString(serialized_graph)
#     tf.import_graph_def(od_graph_def, name='')
#
# sess = tf.Session(graph=detection_graph)

graph = tf.Graph()

graph_def = None
# Extract frozen graph from tar archive.
tar_file = tarfile.open('/Users/stephanie/PycharmProjects/tf-models-master/venv/research/deeplab/pre-trained-models/deeplabv3_pascal_trainval_2018_01_04.tar.gz')
for tar_info in tar_file.getmembers():
    if _FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
        file_handle = tar_file.extractfile(tar_info)
        graph_def = tf.GraphDef.FromString(file_handle.read())
        break

tar_file.close()

if graph_def is None:
    raise RuntimeError('Cannot find inference graph in tar archive.')

with graph.as_default():
    tf.import_graph_def(graph_def, name='')

sess = tf.Session(graph=graph)

for i in os.listdir(IMAGE_DIR):
    folderPath = os.path.join(IMAGE_DIR, i)
    if '.DS_Store' in str(i) or os.path.isdir(folderPath):
        continue

    image = Image.open(IMAGE_DIR+str(i))

    width, height = image.size
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)

    print ('resized image size '+ str(resized_image.size))

    batch_seg_map = sess.run(
                OUTPUT_TENSOR_NAME,
                feed_dict={INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
    seg_map = batch_seg_map[0]

    # Filtering out all other classes excepting Persons
    seg_map[ seg_map > 15 ] = 0
    seg_map[seg_map < 15] = 0

    plt.figure(figsize=(15, 5))
    grid_spec = gridspec.GridSpec(1, 3, width_ratios=[6, 6, 1])

    plt.subplot(grid_spec[0])
    plt.imshow(image)
    plt.axis('off')
    plt.title('input image')

    plt.subplot(grid_spec[1])
    seg_image = get_dataset_colormap.label_to_color_image(
        seg_map, get_dataset_colormap.get_pascal_name()).astype(np.uint8)
    plt.imshow(seg_image)
    plt.axis('off')
    plt.title('segmentation map')

    # plt.subplot(grid_spec[2])
    # plt.imshow(image)
    # plt.imshow(seg_image, alpha=0.7)
    # plt.axis('off')
    # plt.title('segmentation overlay')

    unique_labels = np.unique(seg_map)
    ax = plt.subplot(grid_spec[2])
    plt.imshow(FULL_COLOR_MAP[unique_labels].astype(np.uint8), interpolation='nearest')
    ax.yaxis.tick_right()
    plt.yticks(range(len(unique_labels)), LABEL_NAMES[unique_labels])
    plt.xticks([], [])
    ax.tick_params(width=0)

    plt.show()