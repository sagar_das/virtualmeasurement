import os, argparse
import tensorflow as tf

dir = ''

def freeze_graph(model_dir,output_node_names):
    """Extract the sub graph defined by the output nodes and convert
        all its variables into constant
        Args:
            model_dir: the root folder containing the checkpoint state file
            output_node_names: a string, containing all the output node's names,
                                comma separated
        """
    if not tf.gfile.Exists(model_dir):
        raise AssertionError('Export directory doesn\'t exist')

    if not output_node_names:
        raise AssertionError('You need to supply the name of output name')
    #retrieving checkpoint full path

    checkpoint = tf.train.get_checkpoint_state(model_dir)
    input_checkpoint = checkpoint.model_checkpoint_path

    #frozen graph file full name
    absolute_model_dir = "/".join(input_checkpoint.split('/')[:-1])
    output_graph = absolute_model_dir + "/frozen_model.pb"

    #clearing devices for Tensorflow to control on which device it will load operations
    clear_devices = True

    #Starting session with fresh graph
    with tf.Session(graph=tf.Graph()) as sess:
        #importing the meta graph in the current graph
        saver = tf.train.import_meta_graph(input_checkpoint + '.meta',clear_devices=clear_devices)

        #restoring weights
        saver.restore(sess,input_checkpoint)

        #convert variables to constants
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess,tf.get_default_graph().as_graph_def(),
            output_node_names.split(',')
        )

        #Serializing the graph and dumping to the file system
        with tf.gfile.GFile(output_graph,"wb") as f:
            f.write(output_graph_def.SerializeToString())
        print('%d ops in the final graph' % len(output_graph_def.node))

    return output_graph_def

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", type=str, default="", help="Model folder to export")
    parser.add_argument("--output_node_names", type=str, default="", help="The name of the output nodes, comma separated.")
    args = parser.parse_args()

    freeze_graph(args.model_dir, args.output_node_names)



