import cv2
import numpy as np
import os
import pandas as pd
import math
import argparse
from shapely.geometry import LineString
import imutils as im
import tensorflow as tf
import tarfile
from matplotlib import gridspec
import matplotlib.pyplot as plt
from tf_classification import LabelMapUtils as lm
from tf_classification import VisualizationUtils as vis_util
import csv
from deeplab import get_dataset_colormap
from PIL import Image
from scipy.spatial import distance as dist
import time




#region Command Line Arguments
parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", help="Path of the input images directory")
parser.add_argument("--frozen_graph", help="Path of the frozen graph model")
parser.add_argument("--label_map", help="Path of the label map file")
#parser.add_argument("--output_dir", help="Path of the output directory")
parser.add_argument("--num_output_classes", help="Defines the number of output classes",
                    type=int)
#endregion


# region Variables


args = parser.parse_args()
img_seg_map =None
mLines = []
person_index_list = []

#pixel values
pixel_left_list = []
pixel_right_list = []
pixel_top_list = []
pixel_bottom_list = []
category_list = []
midBotLowerX = 0
midBotLowerY = 0
feetTop = 0
feetBottom = 0
feetLeft = 0
feetRight = 0
lowerBodyFound = False
feetFound = False
pelvisFound = False
personTop = None
personLeft = None
personRight = None
personBottom = None

REFERENCE_OBJECT_WIDTH  = 8
REFERENCE_OBJECT_HEIGHT  = 6
CLOTHES_ADJUSTMENT = 9

ref_obj_height = 0.0
ref_obj_width = 0.0


ref_obj_height_side = 0.0
ref_obj_width_side = 0.0


ellipse_A_w = 0
ellipse_A_h = 0
ellipse_b_w = 0
ellipse_b_h = 0
ellipse_A_w_adjusted = 0
ellipse_A_h_adjusted = 0
ellipse_b_w_adjusted = 0
ellipse_b_h_adjusted = 0


img = None
img_name = None
feet_csv_file = ''
INPUT_TENSOR_NAME = 'ImageTensor:0'
OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
INPUT_SIZE = 513
LABEL_NAMES = np.asarray([
    'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
    'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog',
    'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa',
    'train', 'tv'
])

FULL_LABEL_MAP = np.arange(len(LABEL_NAMES)).reshape(len(LABEL_NAMES), 1)
FULL_COLOR_MAP = get_dataset_colormap.label_to_color_image(FULL_LABEL_MAP)

_TARBALL_NAME = 'deeplab_model.tar.gz'
_FROZEN_GRAPH_NAME = 'frozen_inference_graph'
DEEPLAB_MODEL_TARBALL_PATH = '/Users/stephanie/PycharmProjects/tf-models-master/venv/research/deeplab/pre-trained-models/deeplabv3_pascal_trainval_2018_01_04.tar.gz'



# endregion

def calculateDistance(x1,y1,x2,y2):
    return int(math.sqrt((x2-x1)**2 + (y2-y1)**2))

def order_points(pts):
    # sort the points based on x axis coordinates
    xSorted = pts[np.argsort(pts[:,0]),:]

    # select the left-most and right-most points fom the sorted co-ordinates
    leftMost = xSorted[:2, :]
    rightMost = xSorted[2:, :]

    # sort the left-most coordinates according to their
    # y-coordinates and select the top-left and bottom-left
    # points, respectively
    leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
    (tl, bl) = leftMost

    # now that we have the top-left coordinate, use it as an
    # anchor to calculate the Euclidean distance between the
    # top-left and right-most points; by the Pythagorean
    # theorem, the point with the largest distance will be
    # our bottom-right point
    D = dist.cdist(tl[np.newaxis], rightMost, "euclidean")[0]
    (br, tr) = rightMost[np.argsort(D)[::-1], :]

    # return the coordinates in top-left, top-right,
    # bottom-right, and bottom-left order
    return np.array([tl, tr, br, bl], dtype="float32")


def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)

def readPixelsFromCSV(csv_sheet):
    global category_list,pixel_bottom_list,pixel_left_list,pixel_right_list,pixel_top_list
    pixel_values = pd.read_csv(csv_sheet, delimiter=',')

    category_list = []
    pixel_bottom_list =[]
    pixel_top_list = []
    pixel_left_list = []
    pixel_right_list =[]


    for category in pixel_values.category:
        category_list.append(str(category))
    for left in pixel_values.left:
        pixel_left_list.append(int(left))
    for right in pixel_values.right:
        pixel_right_list.append(int(right))
    for top in pixel_values.top:
        pixel_top_list.append(int(top))
    for bottom in pixel_values.bottom:
        pixel_bottom_list.append(int(bottom))


def line_intersection(l1x1,l1y1,l1x2,l1y2,l2x1,l2y1,l2x2,l2y2):
    line1 = LineString([(l1x1, l1y1), (l1x2, l1y2)])
    line2 = LineString([(l2x1, l2y1), (l2x2, l2y2)])
    print(line1.intersection(line2))
    return line1.intersection(line2)


def detectCurves(gray_image):
    global mLines
    mLines =[]
    gray = cv2.GaussianBlur(gray_image, (5, 5), 0)
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    #edges = cv2.threshold(gray, 45, 255, cv2.THRESH_BINARY)[1]
    edges = cv2.dilate(edges, None, iterations=1)
    edges = cv2.erode(edges, None, iterations=1)

    minLineLength = 130
    maxLineGap = 5
    lines = cv2.HoughLinesP(edges, cv2.HOUGH_PROBABILISTIC, np.pi / 180, 30, minLineLength, maxLineGap)


    for x in range(0, len(lines)):
        for x1, y1, x2, y2 in lines[x]:
            # cv2.line(inputImage,(x1,y1),(x2,y2),(0,128,0),2, cv2.LINE_AA)
            pts = np.array([[x1, y1], [x2, y2]], np.int32)
            mLines.append([x1,y1,x2,y2])
            cv2.polylines(img, [pts], True, (255, 255, 255))

    # sorting lines as per y coordinates
    mLines.sort(key=lambda tp:tp[1])
    # sorting lines as per x coordinates
    # mLines.sort(key=lambda tp:tp[0])
    # print("mLines sorted")
    # print(mLines)
    # print(mLines[0][0])



    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img, "Curves Detected", (500, 250), font, 0.5, 255)

    cv2.imwrite("edges.jpg",edges)

    # gray = cv2.GaussianBlur(opencvOutput, (5, 5), 0)
    # edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    # # edges = cv2.threshold(gray, 45, 255, cv2.THRESH_BINARY)[1]
    # edges = cv2.dilate(edges, None, iterations=1)
    # edges = cv2.erode(edges, None, iterations=1)
    # cv2.imwrite("edges_after.jpg", edges)


def findAfromWaist(IMG_PATH):

    global ellipse_A_h,ellipse_A_w, ellipse_A_h_adjusted,ellipse_A_w_adjusted,img_name,CLOTHES_ADJUSTMENT

    img = cv2.imread(IMG_PATH)
    height, width, channels = img.shape
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    img = cv2.resize(img, target_size)

    lowerBodyFound = False
    if (len(category_list) == 0):
        print('No categories found')
        return
    index = 0
    # find index of lower body
    for category in category_list:
        if 'lower' in str(category):
            # print('Index of lower body is ' + str(index))
            lowerBodyFound = True
            break
        index += 1

    if not lowerBodyFound:
        print('lower body not found in side image')
        exit

    tlX = pixel_left_list[index]
    trX = pixel_right_list[index]
    tlY = pixel_top_list[index]
    trY = pixel_top_list[index]

    midX = int((tlX + trX) / 2)
    midY = int((tlY + trY) / 2)

    waist_index = None
    #Finding waist from segmentation map
    for i in range(midX,len(img_seg_map[0])):
        if img_seg_map[midY][i] == 0:
            break
        waist_index = i

    # Recalculating midpoint along the top line of the bounding box of lower body
    waist_start_index = None
    for i in range(0,len(img_seg_map[0])):
        waist_start_index = i + 1
        if img_seg_map[midY][i] == 15:
            break


    midX = int((waist_index + waist_start_index)/2)

    diff = int((waist_index - waist_start_index) / 2)

    ellipse_a = waist_index-midX
    ellipse_A_w = ellipse_a * (int(ref_obj_width*100)/100)
    ellipse_A_w = round(ellipse_A_w,2)
    ellipse_A_h = ellipse_a * (int(ref_obj_height*100)/100)
    ellipse_A_h = round(ellipse_A_h,2)
    ellipse_A_w_adjusted = (ellipse_a - CLOTHES_ADJUSTMENT) * (int(ref_obj_width * 100) / 100)
    ellipse_A_w_adjusted = round(ellipse_A_w_adjusted,2)
    ellipse_A_h_adjusted = (ellipse_a - CLOTHES_ADJUSTMENT) * (int(ref_obj_height * 100) / 100)
    ellipse_A_h_adjusted = round(ellipse_A_h_adjusted,2)

    print('A for ellipse in pixels' + str(ellipse_a))
    print('A for ellipse with clothes adjustment in pixels' + str(ellipse_a - CLOTHES_ADJUSTMENT))
    #print('A for ellipse in inches (considering ref_obj_width )' + str(ellipse_A_w))
    print('A for ellipse in inches (considering ref_obj_height )' + str(ellipse_A_h))
    #print('A for ellipse with clothes adjustment in inches (considering ref_obj_width )' + str(ellipse_A_w_adjusted))
    print('A for ellipse with clothes adjustment in inches (considering ref_obj_height )' + str(ellipse_A_h_adjusted))


    #cv2.rectangle(img, (tlX, tlY), (trX, trY+50), (0, 255, 0), 3)
    cv2.circle(img, (int(waist_start_index), int(tlY)), 5, (255, 0, 0), -1)
    cv2.circle(img, (int(midX), int(midY)), 5, (255, 0, 0), -1)
    cv2.circle(img, (int(midX + ellipse_a), int(midY)), 5, (255, 0, 0), -1)
    cv2.putText(img, "waist",
                (int(midX + ellipse_a), int(midY - 10)), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (255, 255, 255), 2)

    # for i in eligibleCurves:
    #     cv2.circle(img, (int(i[0]), int(i[1])), 5, (255, 0, 0), -1)
    #     cv2.circle(img, (int(i[2]), int(i[3])), 5, (255, 0, 0), -1)
        #intersectPoints.append(line_intersection(midX,trY,trX,trY,i[0],i[1],i[2],i[3])

    cv2.imwrite('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/results/'+img_name.rstrip('.jpg')+'/'+img_name.rstrip('.jpg')+'_waist.jpg',img)


def findBfromWaist(IMG_PATH):

    global ellipse_b_h,ellipse_b_w, ellipse_b_h_adjusted,ellipse_b_w_adjusted,CLOTHES_ADJUSTMENT

    img = cv2.imread(IMG_PATH)
    height, width, channels = img.shape
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    img = cv2.resize(img, target_size)

    lowerBodyFound = False
    if (len(category_list) == 0):
        print('No categories found')
        return
    index = 0
    # find index of lower body
    for category in category_list:
        if 'lower' in str(category):
            #print('Index of lower body is ' + str(index))
            lowerBodyFound = True
            break
        index += 1

    if not lowerBodyFound:
        print('lower body not found in side image')
        exit

    tlX = pixel_left_list[index]
    trX = pixel_right_list[index]
    tlY = pixel_top_list[index]
    trY = pixel_top_list[index]

    midX = int((tlX + trX)/2)
    midY = int((tlY + trY) / 2)

    waist_index = None
    # Finding waist from segmentation map
    for i in range(midX, len(img_seg_map[0])):
        if img_seg_map[midY][i] == 0:
            break
        waist_index = i

    # Recalculating midpoint along the top line of the bounding box of lower body
    '''Traversing right from top left corner of the bounding box
        towards center
     and finding waist start point'''
    waist_start_index = None
    for i in range(0, len(img_seg_map[0])):
        if img_seg_map[midY][i] == 15:
            break
        waist_start_index = i + 1




    midX = int((waist_index + waist_start_index)/2)

    diff = int((waist_index - waist_start_index)/2)

    ellipse_b = waist_index-midX
    ellipse_b_w = ellipse_b * (int(ref_obj_width_side*100)/100)
    ellipse_b_w = round(ellipse_b_w,2)
    ellipse_b_h = ellipse_b * (int(ref_obj_height_side*100)/100)
    ellipse_b_h = round(ellipse_b_h,2)
    ellipse_b_w_adjusted = (ellipse_b - CLOTHES_ADJUSTMENT) * (int(ref_obj_width_side * 100) / 100)
    ellipse_b_w_adjusted = round(ellipse_b_w_adjusted,2)
    ellipse_b_h_adjusted = (ellipse_b - CLOTHES_ADJUSTMENT) * (int(ref_obj_height_side * 100) / 100)
    ellipse_b_h_adjusted = round(ellipse_b_h_adjusted,2)

    print('B for ellipse in pixels' + str(ellipse_b))
    print('B for ellipse with clothes adjustment in pixels' + str(ellipse_b - CLOTHES_ADJUSTMENT))
    #print('B for ellipse in inches (considering ref_obj_width )' + str(ellipse_b_w))
    print('B for ellipse in inches (considering ref_obj_height )' + str(ellipse_b_h))
    #print('B for ellipse with clothes adjustment in inches (considering ref_obj_width )' + str(ellipse_b_w_adjusted))
    print('B for ellipse with clothes adjustment in inches (considering ref_obj_height )' + str(ellipse_b_h_adjusted))


    #cv2.rectangle(img, (tlX, tlY), (trX, trY+50), (0, 255, 0), 3)
    cv2.circle(img, (int(waist_start_index), int(midY)), 5, (255, 0, 0), -1)
    cv2.circle(img, (int(midX), int(midY)), 5, (255, 0, 0), -1)
    cv2.circle(img, (int(midX + ellipse_b), int(midY)), 5, (255, 0, 0), -1)
    cv2.putText(img, "waist",
                (int(midX + ellipse_b), int(midY - 10)), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (255, 255, 255), 2)

    # for i in eligibleCurves:
    #     cv2.circle(img, (int(i[0]), int(i[1])), 5, (255, 0, 0), -1)
    #     cv2.circle(img, (int(i[2]), int(i[3])), 5, (255, 0, 0), -1)
        #intersectPoints.append(line_intersection(midX,trY,trX,trY,i[0],i[1],i[2],i[3]))

    cv2.imwrite('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/results/'+img_name.rstrip('.jpg')+'/'+img_name.rstrip('.jpg')+'_waist.jpg',img)


def detectFeet():
    global feetTop,feetBottom,feetLeft,feetRight,feetFound,feet_csv_file
    feetFound = False
    pixel_values = pd.read_csv(feet_csv_file, delimiter=',')
    # find index of feet

    if 'feet' in str(pixel_values.category):
        feetFound = True

    if not feetFound:
        print('feet not found')
        return

    feetLeft = int(pixel_values.left[0])
    feetRight = int(pixel_values.right[0])
    feetTop = int(pixel_values.top[0])
    feetBottom = int(pixel_values.bottom[0])



def calculateOutSeam():
    global category_list, pixel_top_list,pixel_bottom_list,pixel_left_list,pixel_right_list, img, midBotLowerX,midBotLowerY, dist, lowerBodyFound
    lowerBodyFound = False
    if(len(category_list)==0):
        print('No categories found')
        return
    index = 0
    #find index of lower body
    for category in category_list:
        if 'lower' in str(category):
            #print('Index of lower body is '+str(index))
            lowerBodyFound = True
            break
        index+=1

    if not lowerBodyFound:
        print('lower body not found')
        exit


    outseam_dist = int(pixel_bottom_list[index]) - int(pixel_top_list[index])
    #print('Up seam in pixels ' + str(int(pixel_bottom_list[index]) - int(pixel_top_list[index])))
    print('Outseam till floor in pixels ' + str(outseam_dist))
    #print('Outseam till floor in inches (considering ref_object_width) ' + str(round((outseam_dist * ref_obj_width),2)))
    print('Outseam till floor in inches (considering ref_object_height) ' + str(round((outseam_dist * ref_obj_height),2)))
    # dA = dist.euclidean((pixel_left_list[index], pixel_bottom_list[index]), (pixel_left_list[index], pixel_top_list[index],))
    #
    # print ('upseam in euclidean '+str(dA))

    # calculate midpoint of top lower body line region
    midX = int((pixel_left_list[index] + pixel_right_list[index]) / 2)
    midY = int((pixel_top_list[index] + pixel_top_list[index]) / 2)

    detectFeet()

    if feetFound:
        # calculate midpoint of feet region by considering a diagonal
        midFeetX = int((feetLeft + feetRight) / 2)
        midFeetY = int((feetTop + feetBottom) / 2)

        # dist = int(math.sqrt((feetLeft - pixel_left_list[index]) ** 2 + (feetTop - pixel_top_list[index]) ** 2))
        # print('New up seam in pixels' + str(dist))

        new_dist = int(math.sqrt((midFeetX - midX) ** 2 + (midFeetY - midY) ** 2))
        print('Outseam till ankle in pixels ' + str(new_dist))
        #print('Outseam till ankle in inches (considering ref_object_width) ' + str(round((new_dist * ref_obj_width),2)))
        print('Outseam till ankle in inches (considering ref_object_height) ' + str(round((new_dist * ref_obj_height),2)))

        cv2.circle(img, (int(midFeetX), int(midFeetY)), 5, (255, 0, 0), -1)

    midBotLowerX = int((pixel_left_list[index] + pixel_right_list[index] ) /2)
    midBotLowerY = int((pixel_bottom_list[index] + pixel_bottom_list[index] ) /2)





def calculateInSeam():
    global category_list, pixel_top_list,pixel_bottom_list,pixel_left_list,pixel_right_list, img, midBotLowerY, midBotLowerX, pelvisFound
    pelvisFound = False
    if(len(category_list)==0):
        print('No categories found')
        return
    index = 0
    #find index of pelvis
    for category in category_list:
        if 'pelvis' in str(category):
            print('Index of pelvis is '+str(index))
            pelvisFound = True
            break
        index+=1

    if not pelvisFound:
        print('pelvis not found')
        return


    #left - X, top - Y
    #right - X, bottom -Y
    # cv2.circle(img, (int(pixel_left_list[index]), int(pixel_top_list[index])), 5, (255, 0, 0), -1)
    # cv2.circle(img, (int(pixel_right_list[index]), int(pixel_bottom_list[index])), 5, (255, 0, 0), -1)

    #calculate midpoint of pelvis region by considering a diagonal
    midX = int((pixel_left_list[index] + pixel_right_list[index] ) /2)
    midY = int((pixel_top_list[index] + pixel_bottom_list[index] ) /2)
    cv2.circle(img, (midX, midY), 5, (255, 0, 0), -1)

    dist = int(math.sqrt((midBotLowerX - midX) ** 2 + (midBotLowerY - midY) ** 2))
    # print('In seam in pixels ' + str(int(midBotLowerY) - int(midY)))
    print('In seam till floor in pixels ' + str(dist))
    #print('In seam till floor in inches (considering ref_object_width) ' + str(dist * ref_obj_width))
    print('In seam till floor in inches (considering ref_object_height) ' + str(dist * ref_obj_height))

    detectFeet()

    if feetFound:
        # calculate midpoint of feet region by considering a diagonal
        midFeetX = int((feetLeft + feetRight) / 2)
        midFeetY = int((feetTop + feetBottom) / 2)
        cv2.circle(img, (midFeetX, midFeetY), 5, (255, 0, 0), -1)


        new_dist = int(math.sqrt((midFeetX - midX)**2 + (midFeetY - midY)**2))
        print('In seam till ankle in pixels ' + str(new_dist))
        #print('In seam till ankle in inches (considering ref_object_width) ' + str(new_dist * ref_obj_width))
        print('In seam till ankle in inches (considering ref_object_height) ' + str(new_dist * ref_obj_height))
        cv2.imwrite('circle.jpg',img)


def calculateWaist():
    p_h = 2 * math.pi * (math.sqrt((ellipse_A_h**2 + ellipse_b_h**2)/2))
    p_w = 2 * math.pi * (math.sqrt((ellipse_A_w**2 + ellipse_b_w**2)/2))
    p_h_a = 2 * math.pi * (math.sqrt((ellipse_A_h_adjusted**2 + ellipse_b_h_adjusted**2)/2))
    p_w_a = 2 * math.pi * (math.sqrt((ellipse_A_w_adjusted**2 + ellipse_b_w_adjusted**2)/2))

    #print('waist in inches (considering ref_obj_width)'+str(round(p_w,2)))
    print('waist in inches (considering ref_obj_height)'+str(round(p_h,2)))
    #print('waist in inches with clothes adjustment (considering ref_obj_width)' + str(round(p_w_a, 2)))
    print('waist in inches with clothes adjustment (considering ref_obj_height)' + str(round(p_h_a, 2)))


#region Refernce Object Detection

def detectReferenceObjectManual():
    img = cv2.imread('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/images/mt_side.jpg')
    height, width, channels = img.shape
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    # image_np = cv2.resize(image, target_size, fx=0.5, fy=0.5, interpolation = cv2.INTER_AREA)
    img = cv2.resize(img, target_size)
    # cv2.circle(img, (848, 1068), 5, (255, 0, 0), -1)
    # cv2.circle(img, (955, 1068), 5, (255, 0, 0), -1)
    #m9
    # cv2.circle(img, (350, 380), 5, (255, 0, 0), -1)
    # cv2.circle(img, (530, 380), 5, (255, 0, 0), -1)
    #m10


    #matt neck front
    # cv2.circle(img, (206, 210), 1, (255, 0, 0), -1)
    # cv2.circle(img, (234, 210), 1, (255, 0, 0), -1)

    #side
    cv2.circle(img, (181, 160), 1, (255, 0, 0), -1)
    cv2.circle(img, (209, 170), 1, (255, 0, 0), -1)

    cv2.imwrite('ref.jpg',img)

def detectReferenceObject():
    image = cv2.imread('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/images/m2_side.jpg')
    height, width, channels = image.shape
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    # image_np = cv2.resize(image, target_size, fx=0.5, fy=0.5, interpolation = cv2.INTER_AREA)
    image = cv2.resize(image, target_size)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (7, 7), 0)

    # detect edges, then perform a dilation + erosion to close gaps between
    # object edges

    edged = cv2.Canny(gray, 50, 100)
    edged = cv2.dilate(edged, None, iterations=1)
    edged = cv2.erode(edged, None, iterations=1)

    # cv2.imshow("edge",edged)
    # cv2.waitKey(0)

    # finding contours in edge map
    cntrs = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cntrs = cntrs[0] if im.is_cv2() else cntrs[1]

    # sort the contours from left-to-right and initialize the bounding box
    # point colors
    (cntrs, _) = sort_contours(cntrs)
    colors = ((0, 0, 255), (240, 0, 159), (255, 0, 0), (255, 255, 0))

    # looping over contours
    for (i, c) in enumerate(cntrs):
        # if contour is relly small then ignore
        if cv2.contourArea(c) < 150:
            continue

        # Compute the rotated bounding box and then draw the contours
        box = cv2.minAreaRect(c)
        box = cv2.cv.BoxPoints(box) if im.is_cv2() else cv2.boxPoints(box)
        box = np.array(box, dtype="int")
        cv2.drawContours(image, [box], -1, (0, 255, 0), 2)

        rect = order_points(box)



        x1 = 0
        y1 = 0
        x2 = 0
        y2 = 0
        x3 = 0
        y3 = 0

        for ((x, y), color) in zip(rect, colors):
            cv2.circle(image, (int(x), int(y)), 5, color, -1)
            print(color)

            if color == (240, 0, 159):
                (x2, y2) = (box[2][0], box[2][1])

            elif color == (255, 0, 0):
                (x3, y3) = (box[1][0], box[1][1])
                #print('height ' + str(calculateDistance(x2, y2, x3, y3)))
            if color == (255,255,0):
                (x1, y1) = (box[0][0], box[0][1])
                #print('width ' + str(calculateDistance(x1, y1, x3, y3)))
            #print('x' + str(x) + ' y' + str(y))

        # draw the object num at the top-left corner
        cv2.putText(image, "Object #{}".format(i + 1),
                    (int(rect[0][0] - 15), int(rect[0][1] - 15)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.55, (255, 255, 255), 2)

        # display original coordinates
        print("Object #{}:".format(i + 1))
        print(box)
        width = box[1][0]-box[0][0]
        height = box[1][1] - box[2][1]
        if width == 0 or height ==0:
            continue
        print('height in pixels'+str(height))
        print('height in inches' + str(round(6/height, 4)))
        print('width in pixels ' + str(width))
        print('width in inches ' + str(round(8/width, 4)))
        # print('rectangle')
        # print(rect)

    # check to see if the new method should be used for
    # ordering the coordinates
    # if args["new"] > 0:
    rect = order_points(box)

    # show the re-ordered coordinates
    print(rect.astype("int"))
    print("")





    # show the image
    # cv2.imshow("Image", image)
    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Image", 600, 400)
    cv2.imshow("Image", image)
    cv2.waitKey(0)


def calculateResultBox(layerOutputs):

    global confidenceCutOff,threshold,boxes,confidences,classIDs

    for output in layerOutputs:
        for detection in output:
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]

            # filter out weak predictions by ensuring the detected
            # probability is greater than the minimum probability

            if confidence > confidenceCutOff:
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")

                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                boxes.append([x,y,int(width),int(height)])
                confidences.append(float(confidence))
                classIDs.append(classID)

    # apply non-maxima suppression to suppress weak, overlapping bounding boxes
    #YOLO does not apply non-maxima suppression for us, so we need to explicitly apply it.
    idxs = cv2.dnn.NMSBoxes(boxes,confidences,confidenceCutOff,threshold)

    return idxs


def drawResultBox(idxs):
    global colors,classIDs,boxes,confidences,image,labels
    if len(idxs) > 0:
        for i in idxs.flatten():
            (x,y) = (boxes[i][0],boxes[i][1])
            (w,h) = (boxes[i][2],boxes[i][3])

            # drawing bounding box rectangle
            color = [int(c) for c in colors[classIDs[i]]]
            cv2.rectangle(image,(x,y),(x+w,y+h),color,2)
            text = "{}: {:.4f}".format(labels[classIDs[i]], confidences[i])
            cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, color, 2)

def sortbyArea(val):
    return val[4]

def detectRefernceAutomated(IMG_PATH):
    global img_name, REFERENCE_OBJECT_WIDTH, REFERENCE_OBJECT_HEIGHT, ref_obj_height, ref_obj_width, ref_obj_height_side, ref_obj_width_side


    img = cv2.imread(IMG_PATH)
    height, width, channels = img.shape
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    # image_np = cv2.resize(image, target_size, fx=0.5, fy=0.5, interpolation = cv2.INTER_AREA)
    img = cv2.resize(img, target_size)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(gray, 127, 255, 1)

    im2, contours, h = cv2.findContours(thresh, 1, 2)

    dtype =[('left',int),('top',int),('right',int),('bottom',int),('area',int)]
    eligibleContourList = np.array([],dtype=dtype)
    eligibleContourList = eligibleContourList.tolist()

    print ('Computing contours for reference object')
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)
        if len(approx) == 4:
            #print('square')
            #print(cnt)
            cv2.drawContours(img, [cnt], 0, (0, 0, 255), -1)

            #Getting left,top,right,bottom coordinates and the area of the contour
            left, top = np.amin(cnt,axis=0)[0][0],np.amin(cnt,axis=0)[0][1]
            right, bottom = np.amax(cnt,axis=0)[0][0], np.amax(cnt,axis=0)[0][1]

            for indexes in person_index_list:
                if (left,top) == (indexes[0],indexes[1]) or (right,bottom) == (indexes[0],indexes[1]):
                    continue

            eligibleContour = [left,top,right,bottom,int(cv2.contourArea(cnt))]
            eligibleContourList.append(eligibleContour)


    #eligibleContourList = reversed(np.sort(eligibleContourList,order='area'))
    eligibleContourList.sort(key=sortbyArea,reverse=True)

    #print(eligibleContourList)
    print('Computing contours for reference object - COMPLETED')

    #zero in the reference object
    ref_obj_left = eligibleContourList[0][0]
    ref_obj_top = eligibleContourList[0][1]
    ref_obj_right = eligibleContourList[0][2]
    ref_obj_bottom = eligibleContourList[0][3]

    cv2.circle(img, (int(ref_obj_left), int(ref_obj_top)), 3, (255, 0, 0), -1)
    cv2.circle(img, (int(ref_obj_right), int(ref_obj_bottom)), 3, (255, 0, 0), -1)
    cv2.putText(img, "Ref object",
                (int((ref_obj_left+ref_obj_right)/2) - 5, int((ref_obj_top+ref_obj_bottom)/2)), cv2.FONT_HERSHEY_SIMPLEX,
                0.3, (255, 255, 255), 1)



    cv2.imwrite('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/results/'+img_name.rstrip('.jpg')+'/'+img_name.rstrip('.jpg')+'_refobject.jpg', img)

    if 'front' in img_name:
        ref_obj_width = REFERENCE_OBJECT_WIDTH/(ref_obj_right - ref_obj_left)
        ref_obj_width = round(ref_obj_width,4)
        ref_obj_height = REFERENCE_OBJECT_HEIGHT/(ref_obj_bottom - ref_obj_top)
        ref_obj_height = round(ref_obj_height,4)
    elif 'side' in img_name:
        ref_obj_width_side = REFERENCE_OBJECT_WIDTH / (ref_obj_right - ref_obj_left)
        ref_obj_width_side = round(ref_obj_width_side,4)
        ref_obj_height_side = REFERENCE_OBJECT_HEIGHT / (ref_obj_bottom - ref_obj_top)
        ref_obj_height_side = round(ref_obj_height_side,4)

    
    




#endregion


#region Image Classification
def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


def detectFeetBoundingBoxes(image_path, result_path, csv_file_name):
    global img,INPUT_SIZE

    print('Detecting feet')

    PATH_TO_CKPT = '/Users/stephanie/PycharmProjects/tf-models-master/venv/workspace_training/training_demo/training_feet_03022019_1200/frozen_inference_graph.pb'
    PATH_TO_LABELS = '/Users/stephanie/PycharmProjects/tf-models-master/venv/workspace_training/training_demo/annotations/label_feet_map.pbtxt'
    NUM_CLASSES = 1

    # Creating Image file specific file

    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map = lm.load_labelmap(PATH_TO_LABELS)
    categories = lm.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                    use_display_name=True)
    category_index = lm.create_category_index(categories)

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

            # Each box represents a part of the image where a particular object was detected.
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class label.

            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')

            image_np = cv2.imread(image_path, 1)

            height, width, channels = image_np.shape
            resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
            target_size = (int(resize_ratio * width), int(resize_ratio * height))
            #image_np = cv2.resize(image, target_size, fx=0.5, fy=0.5, interpolation = cv2.INTER_AREA)
            image_np = cv2.resize(image_np, target_size)

            img = image_np

            # the array based representation of the image will be used later in order to prepare the
            # result image with boxes and labels on it.

            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            image_np_expanded = np.expand_dims(image_np, axis=0)

            # Actual detection.
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            # Visualization of the results of a detection.

            vis_util.visualize_boxes_and_labels_on_image_array(
                csv_file_name,
                image_path,
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
            )

            #image_np = cv2.resize(image_np, (width, height))
            cv2.imwrite(result_path, image_np)

    print('Detecting feet - COMPLETED')

def detectBoundingBoxes(image_path, result_path,csv_file_name):
    
    global img, INPUT_SIZE
    
    PATH_TO_CKPT = args.frozen_graph
    PATH_TO_LABELS = args.label_map
    NUM_CLASSES = args.num_output_classes

    print ('Detecting Torso, Head, Lower body')
    
    #Creating Image file specific file

    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map = lm.load_labelmap(PATH_TO_LABELS)
    categories = lm.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = lm.create_category_index(categories)

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

            # Each box represents a part of the image where a particular object was detected.
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class label.

            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
            
            image_np = cv2.imread(image_path, 1)

            height, width, channels = image_np.shape
            resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
            target_size = (int(resize_ratio * width), int(resize_ratio * height))
            # image_np = cv2.resize(image, target_size, fx=0.5, fy=0.5, interpolation = cv2.INTER_AREA)
            image_np = cv2.resize(image_np, target_size)

            img = image_np

            # the array based representation of the image will be used later in order to prepare the
            # result image with boxes and labels on it.

            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            image_np_expanded = np.expand_dims(image_np, axis=0)

            # Actual detection.
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            # Visualization of the results of a detection.

            vis_util.visualize_boxes_and_labels_on_image_array(
                csv_file_name,
                image_path,
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
            )

            #image_np = cv2.resize(image_np, (width,height))
            cv2.imwrite(result_path, image_np)

    print ('Detecting Torso, Head, Lower body - COMPLETED')





#endregion

#region Image Segmentation
def detectSegmetMap(IMAGE_PATH):
    global img_seg_map,img_name,person_index_list

    print('Creating segment map')
    graph = tf.Graph()

    graph_def = None
    # Extract frozen graph from tar archive.
    tar_file = tarfile.open(DEEPLAB_MODEL_TARBALL_PATH)
    for tar_info in tar_file.getmembers():
        if _FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
            file_handle = tar_file.extractfile(tar_info)
            graph_def = tf.GraphDef.FromString(file_handle.read())
            break

    tar_file.close()

    if graph_def is None:
        raise RuntimeError('Cannot find inference graph in tar archive.')

    with graph.as_default():
        tf.import_graph_def(graph_def, name='')

    sess = tf.Session(graph=graph)


    image = Image.open(IMAGE_PATH)

    width, height = image.size
    resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)

    batch_seg_map = sess.run(
        OUTPUT_TENSOR_NAME,
        feed_dict={INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
    seg_map = batch_seg_map[0]

    # Filtering out all other classes excepting Persons
    seg_map[seg_map > 15] = 0
    seg_map[seg_map < 15] = 0

    img_seg_map = seg_map

    h,w = img_seg_map.shape



    for i in range(0,h-1):
        for j in range(0,w-1):
            if img_seg_map[i][j] == 15:
                person_index_list.append([i,j])

    #print(person_index_list)

    # personLeft = index_list[0][0]
    # personTop = index_list[0][1]
    # personRight = index_list[len(index_list)-1][0]
    # personBottom = index_list[len(index_list)-1][1]


    plt.figure(figsize=(15, 5))
    grid_spec = gridspec.GridSpec(1, 4, width_ratios=[6, 6, 6, 1])

    plt.subplot(grid_spec[0])
    plt.imshow(image)
    plt.axis('off')
    plt.title('input image')

    plt.subplot(grid_spec[1])
    image_class = Image.open('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/results/'+img_name.rstrip('.jpg')+'/'+img_name)
    plt.imshow(image_class)
    plt.axis('off')
    plt.title('classification map')

    plt.subplot(grid_spec[2])
    seg_image = get_dataset_colormap.label_to_color_image(
        seg_map, get_dataset_colormap.get_pascal_name()).astype(np.uint8)
    plt.imshow(seg_image)
    plt.axis('off')
    plt.title('segmentation map')

    unique_labels = np.unique(seg_map)
    ax = plt.subplot(grid_spec[3])
    plt.imshow(FULL_COLOR_MAP[unique_labels].astype(np.uint8), interpolation='nearest')
    ax.yaxis.tick_right()
    plt.yticks(range(len(unique_labels)), LABEL_NAMES[unique_labels])
    plt.xticks([], [])
    ax.tick_params(width=0)

    #plt.show()

    print('Creating segment map - COMPLETED')

    plt.savefig('/Users/stephanie/PycharmProjects/VirtualMeasurement/venv/results/'+img_name.rstrip('.jpg')+'/'+img_name.rstrip('.jpg')+'_segment.jpg')


#endregion


#detectReferenceObject()
# detectReferenceObjectManual()
# circum = p_h = 2 * math.pi * (math.sqrt((2.70**2 + 2.87**2)/2))
# print(circum)



# Measurement process
PATH_TO_TEST_IMAGES_DIR = args.input_dir
for i in os.listdir(PATH_TO_TEST_IMAGES_DIR):
    folderPath = os.path.join(PATH_TO_TEST_IMAGES_DIR, i)

    if '.DS_Store' in str(i) or os.path.isdir(folderPath):
        continue

    img_name = str(i)

    resDirName = './results/'+str(i).rstrip('.jpg')
    if not os.path.exists(resDirName):
        os.mkdir(resDirName)

    field_names = ['path', 'category', 'left', 'right', 'top', 'bottom']
    csv_file_name =resDirName+'/pixel_values_'+str(i).rstrip('.jpg')+'.csv'
    with open(csv_file_name, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=field_names)
        writer.writeheader()

    detectBoundingBoxes(PATH_TO_TEST_IMAGES_DIR+'/'+str(i),resDirName+'/'+str(i),csv_file_name)
    detectSegmetMap(PATH_TO_TEST_IMAGES_DIR+'/'+str(i))
    detectRefernceAutomated(PATH_TO_TEST_IMAGES_DIR+'/'+str(i))

    readPixelsFromCSV(csv_file_name)

    #Measuring from front view
    if 'front' in str(i):
        field_names = ['path', 'category', 'left', 'right', 'top', 'bottom']
        csv_file_name = resDirName + '/pixel_values_' + str(i).rstrip('.jpg')+'_feet' + '.csv'
        with open(csv_file_name, 'w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=field_names)
            writer.writeheader()
        detectFeetBoundingBoxes(PATH_TO_TEST_IMAGES_DIR+'/'+str(i),resDirName+'/'+str(i).rstrip('.jpg')+'_feet.jpg',
                                csv_file_name)
        feet_csv_file = csv_file_name

        calculateOutSeam()
        calculateInSeam()
        findAfromWaist(PATH_TO_TEST_IMAGES_DIR+'/'+str(i))

    if 'side' in str(i):
        pass
        findBfromWaist(PATH_TO_TEST_IMAGES_DIR+'/'+str(i))


calculateWaist()






