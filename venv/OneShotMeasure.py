import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)

learning_rate = 0.01
num_of_epochs = 100

input_layer=''

#Convolution layer 1
conv1 = tf.layers.conv2d(
    inputs=input_layer,
    filters=20,
    kernel=[5,5],
    padding='same',
    activation=tf.nn.relu

)

#Pooling layer 1
pool1 = tf.layers.max_pooling2d(inputs=conv1,pool_size=[2,2],stride=2)

#Convolution layer 2
conv2 = tf.layers.conv2d(
    inputs=pool1,
    filters=48,
    kernel=[5,5],
    padding='same',
    activation=tf.nn.relu

)

#Pooling layer 2
pool2 = tf.layers.max_pooling2d(inputs=conv2,pool_size=[2,2],stride=2)

#convolution layer 3
conv3 = tf.layers.conv2d(
    inputs=pool2,
    filters=64,
    kernel=[3,3],
    padding = 'same',
    activation=tf.nn.relu
)

#dense layer 1

fully_conn1 = tf.layers.dense(inputs=conv3,units=512,activation=tf.nn.relu)